INSERT INTO country (country_id, country_code) VALUES (1, 'US');
INSERT INTO country (country_id, country_code) VALUES (2, 'UA');
INSERT INTO country (country_id, country_code) VALUES (3, 'RU');
INSERT INTO country (country_id, country_code) VALUES (4, 'UK');

INSERT INTO company (company_id, name, front_devs_count, back_devs_count, country_id) VALUES (1, 'apple', 10, 8, 1);
INSERT INTO company (company_id, name, front_devs_count, back_devs_count, country_id) VALUES (2, 'evenmore', 4, 10, 2);
INSERT INTO company (company_id, name, front_devs_count, back_devs_count, country_id) VALUES (3, 'yandex', 7, 15, 3);
INSERT INTO company (company_id, name, front_devs_count, back_devs_count, country_id) VALUES (4, 'virgin', 5, 7, 4);