package com.evenmore.predictor.controllers;

import com.evenmore.predictor.domain.Company;
import com.evenmore.predictor.dto.PredictProjectEndDateRequestDto;
import com.evenmore.predictor.dto.PredictProjectEndDateResponseDto;
import com.evenmore.predictor.services.CompanyService;
import com.evenmore.predictor.services.PredictionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;

@RestController
public class CompanyController {

    private PredictionService predictionService;

    private CompanyService companyService;

    @Autowired
    public CompanyController(PredictionService predictionService, CompanyService companyService) {
        this.predictionService = predictionService;
        this.companyService = companyService;
    }

    @PostMapping(value = "/companies")
    public ResponseEntity save(@RequestBody Company company) {
        companyService.save(company);
        return ResponseEntity.ok(company);
    }

    @DeleteMapping(value = "/companies")
    public ResponseEntity deleteAll() {
        companyService.deleteAll();
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/companies/predictProjectEndDate")
    public PredictProjectEndDateResponseDto predictProjectEndDate(@RequestBody PredictProjectEndDateRequestDto predictProjectEndDateDto) throws URISyntaxException {
        return predictionService.predictProjectEndDate(predictProjectEndDateDto.getFrontEstimatedHours(), predictProjectEndDateDto.getBackEstimatedHours());
    }
}
