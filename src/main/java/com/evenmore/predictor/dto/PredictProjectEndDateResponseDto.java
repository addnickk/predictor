package com.evenmore.predictor.dto;

import lombok.Data;

@Data
public class PredictProjectEndDateResponseDto {

    public PredictProjectEndDateResponseDto(String projectEndDate, String companyName) {
        this.projectEndDate = projectEndDate;
        this.companyName = companyName;
    }

    private String projectEndDate;

    private String companyName;
}
