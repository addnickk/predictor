package com.evenmore.predictor.dto;

import lombok.Data;

@Data
public class PredictProjectEndDateRequestDto {

    private Integer frontEstimatedHours;

    private Integer backEstimatedHours;
}
