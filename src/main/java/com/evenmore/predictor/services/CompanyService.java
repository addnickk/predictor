package com.evenmore.predictor.services;

import com.evenmore.predictor.domain.Company;
import com.evenmore.predictor.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CompanyService {

    private CompanyRepository companyRepository;

    @Autowired
    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public void save(Company company) {
        companyRepository.save(company);
    }

    public Optional<Company> findById(Long companyId) {
        return companyRepository.findById(companyId);
    }

    public Iterable<Company> findAll() {
        return companyRepository.findAll();
    }

    public void deleteAll() {
        companyRepository.deleteAll();
    }
}
