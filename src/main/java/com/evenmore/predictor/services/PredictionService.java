package com.evenmore.predictor.services;

import com.evenmore.predictor.domain.Company;
import com.evenmore.predictor.dto.PredictProjectEndDateResponseDto;
import org.apache.http.client.utils.URIBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class PredictionService {

    private CompanyService companyService;

    private RestTemplate restTemplate;

    private static final String HOLIDAY_SERVICE_URL = "https://calendarific.com/api/v2/holidays";

    private static final String DATE_FORMAT = "yyyy.MM.dd";

    @Value("${api.key}")
    private String apiKey;

    @Autowired
    public PredictionService(CompanyService companyService, RestTemplate restTemplate) {
        this.companyService = companyService;
        this.restTemplate = restTemplate;
    }

    public PredictProjectEndDateResponseDto predictProjectEndDate(int frontEstimatedHours, int backEstimatedHours) throws URISyntaxException {
        Calendar projectEndDate = null;
        Long bestMatchCompanyId = null;
        Calendar companyBookedUntilDate = null;
        Iterable<Company> companies = companyService.findAll();

        for (Company company : companies) {
            List<Calendar> holidays;
            Calendar endDate = company.getBookedUntilDate() == null || company.getBookedUntilDate().before(Calendar.getInstance()) ? Calendar.getInstance() : (Calendar) company.getBookedUntilDate().clone();

            double frontDaysOfWork = (double) frontEstimatedHours / (company.getFrontDevsCount() * 8);
            double backDaysOfWork = (double) backEstimatedHours / (company.getBackDevsCount() * 8);
            double daysOfWork = frontDaysOfWork > backDaysOfWork ? frontDaysOfWork : backDaysOfWork;


            int holidaysYear = Calendar.getInstance().get(Calendar.YEAR);
            holidays = getHolidaysForYearAndCountry(holidaysYear, company.getCountry().getCountryCode());

            while (daysOfWork >= 1) {
                int endDateYear = endDate.get(Calendar.YEAR);
                if (endDateYear != holidaysYear) {
                    holidaysYear = endDateYear;
                    holidays = getHolidaysForYearAndCountry(holidaysYear, company.getCountry().getCountryCode());
                }
                endDate.add(Calendar.DATE, 1);
                int endDateDay = endDate.get(Calendar.DAY_OF_WEEK);
                if (endDateDay == Calendar.SATURDAY || endDateDay == Calendar.SUNDAY || isDayHoliday(holidays, endDate)) {
                    continue;
                }
                daysOfWork--;
            }

            if (projectEndDate == null || projectEndDate.after(endDate)) {
                projectEndDate = endDate;
                bestMatchCompanyId = company.getCompanyId();
                companyBookedUntilDate = endDate;
            }
        }

        Company bookedCompany = companyService.findById(bestMatchCompanyId).get();
        bookedCompany.setBookedUntilDate(companyBookedUntilDate);
        companyService.save(bookedCompany);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);

        PredictProjectEndDateResponseDto response = new PredictProjectEndDateResponseDto(simpleDateFormat.format(projectEndDate.getTime()), companyService.findById(bestMatchCompanyId).get().getName());
        return response;
    }

    private List<Calendar> getHolidaysForYearAndCountry(int year, String countryCode) throws URISyntaxException {
        ArrayList<Calendar> holidays = new ArrayList<>();

        URIBuilder serviceURI = new URIBuilder(HOLIDAY_SERVICE_URL);
        serviceURI.addParameter("api_key", apiKey);
        serviceURI.addParameter("country", countryCode);
        serviceURI.addParameter("year", String.valueOf(year));

        ResponseEntity<String> response
                = restTemplate.getForEntity(serviceURI.toString(), String.class);

        JSONArray holidaysJsonArray = new JSONObject(response.getBody()).getJSONObject("response").getJSONArray("holidays");
        for (Object holidayObj : holidaysJsonArray) {
            JSONObject holidayJson = (JSONObject) holidayObj;
            JSONObject holidayDateJson = holidayJson.getJSONObject("date").getJSONObject("datetime");

            Calendar holidayDate = Calendar.getInstance();
            holidayDate.set(Calendar.YEAR, holidayDateJson.optInt("year"));
            holidayDate.set(Calendar.MONTH, holidayDateJson.optInt("month") - 1);
            holidayDate.set(Calendar.DAY_OF_MONTH, holidayDateJson.optInt("day"));

            holidays.add(holidayDate);
        }

        return holidays;
    }

    private boolean isDayHoliday(List<Calendar> holidays, Calendar day) {
        return holidays.stream().anyMatch(holiday -> (holiday.get(Calendar.YEAR) == day.get(Calendar.YEAR)
                && holiday.get(Calendar.MONTH) == day.get(Calendar.MONTH)
                && holiday.get(Calendar.DAY_OF_MONTH) == day.get(Calendar.DAY_OF_MONTH)));
    }
}
