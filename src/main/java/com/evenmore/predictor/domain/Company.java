package com.evenmore.predictor.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Calendar;

@Entity
@Data
@EqualsAndHashCode(exclude = "country")
public class Company implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long companyId;

    private String name;

    private Integer frontDevsCount;

    private Integer backDevsCount;

    private Calendar bookedUntilDate;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;
}
