package com.evenmore.predictor.repository;

import com.evenmore.predictor.domain.Company;
import org.springframework.data.repository.CrudRepository;

public interface CompanyRepository extends CrudRepository<Company, Long> {
}
