package com.evenmore.predictor.services;

import com.evenmore.predictor.domain.Company;
import com.evenmore.predictor.domain.Country;
import com.evenmore.predictor.dto.PredictProjectEndDateResponseDto;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
public class PredictionServiceTest {

    @InjectMocks
    private PredictionService predictionService;

    @Mock
    private CompanyService companyService;

    @Mock
    private RestTemplate restTemplate;

    private Company buildCompany(Long id, String name, int frontDevsCount, int backDevsCount, Calendar bookedUntilDate) {
        Company company = new Company();
        company.setCompanyId(id);
        company.setName(name);
        company.setFrontDevsCount(frontDevsCount);
        company.setBackDevsCount(backDevsCount);
        company.setBookedUntilDate(bookedUntilDate);

        Country usa = new Country();
        usa.setCountryCode("US");
        company.setCountry(usa);

        return company;
    }

    private ResponseEntity buildHolidaysResponse() {
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(new JSONObject().put("date", new JSONObject().put("datetime", new JSONObject().put("year", 2019).put("month", 2).put("day", 18))));
        JSONObject body = new JSONObject().put("response", new JSONObject().put("holidays", jsonArray));
        return new ResponseEntity(body.toString(), HttpStatus.OK);
    }

    @Test
    public void shouldPredictProjectEndDateBySingleCompanyForEstimatedHours() throws Exception {
        //given
        Integer frontEstimatedHours = 400;
        Integer backendEstimatedHours = 100;

        Company apple = buildCompany(1L, "Apple", 10, 8, null);

        List<Company> companies = Collections.singletonList(apple);

        when(companyService.findById(1L)).thenReturn(Optional.of(apple));
        when(companyService.findAll()).thenReturn(companies);
        when(restTemplate.getForEntity(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(buildHolidaysResponse());

        //when
        PredictProjectEndDateResponseDto result = predictionService.predictProjectEndDate(frontEstimatedHours, backendEstimatedHours);

        //then
        String expectedResult = "2019.02.25";
        String actualResult = result.getProjectEndDate();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void shouldPredictProjectEndDateAndBestMatchCompanyForEstimatedHours() throws URISyntaxException {
        //given
        Integer frontEstimatedHours = 100;
        Integer backendEstimatedHours = 400;

        Company apple = buildCompany(1L, "Apple", 10, 8, null);
        Company evenmore = buildCompany(2L, "EvenMore", 8, 10, null);

        List<Company> companies = Arrays.asList(apple, evenmore);

        when(companyService.findById(1L)).thenReturn(Optional.of(apple));
        when(companyService.findById(2L)).thenReturn(Optional.of(evenmore));
        when(companyService.findAll()).thenReturn(companies);
        when(restTemplate.getForEntity(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(buildHolidaysResponse());

        //when
        PredictProjectEndDateResponseDto result = predictionService.predictProjectEndDate(frontEstimatedHours, backendEstimatedHours);

        //then
        String expectedDateResult = "2019.02.25";
        String actualDateResult = result.getProjectEndDate();

        String expectedCompanyNameResult = "EvenMore";
        String actualCompanyNameResult = result.getCompanyName();

        assertEquals(expectedDateResult, actualDateResult);
        assertEquals(expectedCompanyNameResult, actualCompanyNameResult);
    }

    @Test
    public void shouldPredictProjectEndDateAndBestMatchCompanyForEstimatedHoursIfCompanyIsBooked() throws URISyntaxException {
        //given
        Integer frontEstimatedHours = 100;
        Integer backendEstimatedHours = 400;

        Company apple = buildCompany(1L, "Apple", 10, 8, null);
        Company evenmore = buildCompany(2L, "EvenMore", 8, 10, new GregorianCalendar(2019, Calendar.MARCH, 1));

        List<Company> companies = Arrays.asList(apple, evenmore);

        when(companyService.findById(1L)).thenReturn(Optional.of(apple));
        when(companyService.findById(2L)).thenReturn(Optional.of(evenmore));
        when(companyService.findAll()).thenReturn(companies);
        when(restTemplate.getForEntity(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(buildHolidaysResponse());

        //when
        PredictProjectEndDateResponseDto result = predictionService.predictProjectEndDate(frontEstimatedHours, backendEstimatedHours);

        //then
        String expectedDateResult = "2019.02.26";
        String actualDateResult = result.getProjectEndDate();

        String expectedCompanyNameResult = "Apple";
        String actualCompanyNameResult = result.getCompanyName();

        assertEquals(expectedDateResult, actualDateResult);
        assertEquals(expectedCompanyNameResult, actualCompanyNameResult);
    }

    @Test
    public void shouldPredictSoonestProjectEndDateAndBestMatchCompanyForEstimatedHoursIfAllCompaniesAreBooked() throws URISyntaxException {
        //given
        Integer frontEstimatedHours = 100;
        Integer backendEstimatedHours = 400;

        Company apple = buildCompany(1L, "Apple", 10, 8, new GregorianCalendar(2019, Calendar.FEBRUARY, 19));
        Company evenmore = buildCompany(2L, "EvenMore", 8, 10, new GregorianCalendar(2019, Calendar.MARCH, 1));

        List<Company> companies = Arrays.asList(apple, evenmore);

        when(companyService.findById(1L)).thenReturn(Optional.of(apple));
        when(companyService.findById(2L)).thenReturn(Optional.of(evenmore));
        when(companyService.findAll()).thenReturn(companies);
        when(restTemplate.getForEntity(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(buildHolidaysResponse());

        //when
        PredictProjectEndDateResponseDto result = predictionService.predictProjectEndDate(frontEstimatedHours, backendEstimatedHours);

        //then
        String expectedDateResult = "2019.02.27";
        String actualDateResult = result.getProjectEndDate();

        String expectedCompanyNameResult = "Apple";
        String actualCompanyNameResult = result.getCompanyName();

        assertEquals(expectedDateResult, actualDateResult);
        assertEquals(expectedCompanyNameResult, actualCompanyNameResult);
    }

}