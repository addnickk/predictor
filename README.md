* It is simple Spring Boot Application
you can run it via console:
mvn install,
java -jar target/predictor-0.0.1-SNAPSHOT.jar
or use IntellijIDEA

* API endpoints:
1) localhost:8080/api/companies/predictProjectEndDate use POST for predict project end date,
body: {"frontEstimatedHours": x, "backEstimatedHours": y} 
2) localhost:8080/api/companies use DELETE for deleting all companies

It is also deployed on heroku and is accessible by link https://young-refuge-41953.herokuapp.com/

